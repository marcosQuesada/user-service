package grpc

import (
	"context"
	"fmt"
	client "bitbucket.org/marcosQuesada/common/transport/grpc/user/client"
	server "bitbucket.org/marcosQuesada/user-service/cmd"
	domain "bitbucket.org/marcosQuesada/user-service/pkg/domain/user"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"testing"
	"time"
)

func TestGetUserOnUserFound(t *testing.T) {
	port := 50050
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	var uid = "ef2acb39-5f44-4e1b-a045-41988f9b94df"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	u := &domain.User{
		ID:      uid,
		Token:   "fakeToken",
		Name:    "foo",
		State:   domain.ENABLED,
		Role:    domain.PREMIUM,
		Created: time.Now(),
	}

	_, err = cl.Register(context.Background(), u)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	u, err = cl.GetUser(context.Background(), uid)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	if u.Name != "foo" {
		t.Errorf("Unexpected result, expected %s got %s", "fooName", u.Name)
	}
}

func TestGetUserOnUserNotFound(t *testing.T) {
	port := 50051
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	var uid = "fake"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	u, err := cl.GetUser(context.Background(), uid)
	if err == nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	if u != nil {
		t.Error("Unexpected result")
	}
}

func TestGetAllUsers(t *testing.T) {
	port := 50052
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	cl, err := client.NewClient(endpoint)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	u, err := cl.GetAll(context.Background())
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	if len(u) == 0 {
		t.Errorf("Unexpected result size,  got %d", len(u))
	}
}

func TestAuthorizationRegisterAndDeleteUser(t *testing.T) {
	port := 50053
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	var uid = "b2ca721b-116e-4d05-9ad9-b846b517752a"
	var token = "basicToken"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	u := &domain.User{
		ID:      uid,
		Token:   token,
		Name:    "foo",
		State:   domain.ENABLED,
		Role:    domain.PREMIUM,
		Created: time.Now(),
	}
	res, err := cl.Register(context.Background(), u)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	if !res {
		t.Error("Unexpected result, expected auth success")
	}

	uu, err := cl.GetUser(context.Background(), u.ID)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	if uu.Name != u.Name {
		t.Errorf("unexpected result, expected %s got %s", u.Name, uu.Name)
	}

	err = cl.Delete(context.Background(), u.ID)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}
}

func TestUserUpdate(t *testing.T) {
	port := 50054
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	var uid = "af2acb39-5f44-4e1b-a045-41988f9b94df"
	var token = "HashedPass"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	u := &domain.User{
		ID:      uid,
		Token:   token,
		Name:    "UpdatedFooName",
		State:   domain.ENABLED,
		Role:    domain.PREMIUM,
		Created: time.Now(),
	}

	err = cl.Update(context.Background(), u)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	if err != nil {
		t.Error("Unexpected result, expected auth success")
	}

	uu, err := cl.GetUser(context.Background(), u.ID)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	if uu.Name != u.Name {
		t.Errorf("Unexpected result, expected %s got %s", u.Name, uu.Name)
	}
}

func TestAuthorizationCallOnSuccessCredentials(t *testing.T) {
	port := 50055
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	var uid = "ff2acb39-5f44-4e1b-a045-41988f9b94df"
	var token = "basicToken"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	u := &domain.User{
		ID:      uid,
		Token:   token,
		Name:    "foo",
		State:   domain.ENABLED,
		Role:    domain.PREMIUM,
		Created: time.Now(),
	}
	_, err = cl.Register(context.Background(), u)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	res, err := cl.Authenticate(context.Background(), uid, token)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	if !res {
		t.Error("Unexpected result, expected auth success")
	}
}

func TestAuthorizationCallOnUnSuccessCredentials(t *testing.T) {
	port := 50056
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	var uid = "af2acb39-5f44-4e1b-a045-41988f9b94df"
	var token = "NonValid"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	res, err := cl.Authenticate(context.Background(), uid, token)
	if err != nil {
		log.Fatalf("Unexpected error, %v", err)
	}

	if res {
		t.Error("Unexpected result, expected auth success")
	}
}

func runServer() error {
	c := server.GetCmd()

	cc := &cobra.Command{
		Use:   "user-service",
		Short: "user-service cli",
		Long:  "user-service CLI",
	}

	go c.Run(cc, []string{})

	return nil
}
