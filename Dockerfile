# Container to compile the app
FROM golang:1.11-alpine AS build

RUN mkdir -p /go/src/bitbucket.org/marcosQuesada/user-service
ADD . /go/src/bitbucket.org/marcosQuesada/user-service
WORKDIR /go/src/bitbucket.org/marcosQuesada/user-service

RUN go build -o /bin/user-service

# Final container image
FROM alpine
RUN apk --update upgrade && \
    apk add curl ca-certificates && \
    update-ca-certificates && \
    rm -rf /var/cache/apk/*
COPY --from=build /bin/user-service /bin/user-service

EXPOSE 50050
ENTRYPOINT ["/bin/user-service"]
CMD ["grpc", "--psql-host", "postgresdb"]