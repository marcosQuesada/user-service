package user

import (
	"context"
	"testing"
	"time"

	_ "github.com/lib/pq"
	"bitbucket.org/marcosQuesada/user-service/test"
)

func TestGetUserByUid(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	uid := "af2acb39-5f44-4e1b-a045-41988f9b94df"
	repo := NewSQLRepository(db)

	user, err := repo.GetByUid(context.Background(), uid)
	if err != nil {
		t.Fatalf("Unexpected error geting user by id, %s", err)
	}

	name := "fooName"
	if user.Name != name {
		t.Errorf("Unexpected result, expected %s got %s", name, user.Name)
	}
}

func TestGetAllUsers(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)
	u, err := repo.GetAll(context.Background())
	if err != nil {
		t.Fatalf("Unexpected error geting u by id, %s", err)
	}

	size := 2
	if len(u) != size {
		t.Errorf("Unexpected result, expected %d got %d", size, len(u))
	}
}

func TestAddUser(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)
	u := &User{
		ID:      "b304623a-6476-4991-8476-3b62a29484c4 ",
		Token:   "Hashed Token",
		Name:    "fooName",
		State:   ENABLED,
		Role:    ADMIN,
		Created: time.Now(),
	}

	err = repo.Add(context.Background(), u)
	if err != nil {
		t.Fatalf("Unexpected error inserting user, %s", err)
	}

	user, err := repo.GetByUid(context.Background(), u.ID)
	if err != nil {
		t.Fatalf("Unexpected error geting user, %v", err)
	}

	if user.Name != u.Name {
		t.Fatalf("Unexpected result, expected %s got %s", u.Name, user.Name)
	}

}
func TestDeleteUser(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	uid := "af2acb39-5f44-4e1b-a045-41988f9b94df"
	repo := NewSQLRepository(db)
	err = repo.Delete(context.Background(), uid)
	if err != nil {
		t.Fatalf("Unexpected error inserting user, %s", err)
	}

	users, err := repo.GetAll(context.Background())
	if err != nil {
		t.Fatalf("Unexpected error geting user, %v", err)
	}

	if len(users) != 1 {
		t.Fatalf("Unexpected user lenght, expected %d got %d", 2, len(users))
	}
}
