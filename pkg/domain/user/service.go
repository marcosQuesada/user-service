package user

import (
	"context"
	log "github.com/sirupsen/logrus"
)

type Repository interface {
	GetByUid(ctx context.Context, uid string) (*User, error)
	GetAll(ctx context.Context) ([]*User, error)
	Add(ctx context.Context, u *User) error
	Update(ctx context.Context, u *User) error
	Delete(ctx context.Context, uid string) error
}

type Service interface {
	GetUser(ctx context.Context, uid string) (*User, error)
	GetAll(ctx context.Context) ([]*User, error)
	Authenticate(ctx context.Context, uid string, token string) (bool, error)
	Register(ctx context.Context, user *User) error
	Update(ctx context.Context, user *User) error
	Delete(ctx context.Context, uid string) error
}

type DefaultService struct {
	repository Repository
}

func NewService(r Repository) *DefaultService {
	return &DefaultService{
		repository: r,
	}
}

func (s *DefaultService) GetUser(ctx context.Context, uid string) (*User, error) {
	return s.repository.GetByUid(ctx, uid)
}


func (s *DefaultService) GetAll(ctx context.Context) ([]*User, error) {
	return  s.repository.GetAll(ctx)
}


func (s *DefaultService) Authenticate(ctx context.Context, uid string, token string) (bool, error) {
	u, err := s.repository.GetByUid(ctx, uid)
	if err != nil {
		return false, err
	}

	if u == nil {
		return false, nil
	}

	if u.Token != token {
		log.Debugf("token do not match, user %s token %s, token found %s", uid, token, u.Token)
		return false, nil
	}

	return true, nil
}

func (s *DefaultService) Register(ctx context.Context, user *User) error {
	return s.repository.Add(ctx, user)
}

func (s *DefaultService) Update(ctx context.Context, user *User) error {
	return s.repository.Update(ctx, user)
}

func (s *DefaultService) Delete(ctx context.Context, uid string) error {
	return s.repository.Delete(ctx, uid)
}
