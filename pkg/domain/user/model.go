package user

import "time"

type StateType int

const (
	ENABLED StateType = iota
	DISABLED
)

type RoleType int

const (
	BASIC RoleType = iota
	SILVER
	GOLD
	PREMIUM
	ADMIN
)

type User struct {
	ID      string    `db:"id" json:"id"` //v4 uuid
	Token   string    `db:"pass" json:"pass"`
	Name    string    `db:"username" json:"username"`
	State   StateType `db:"state" json:"state"`
	Role    RoleType  `db:"role" json:"role"`
	Created time.Time `db:"created" json:"created"`
}
