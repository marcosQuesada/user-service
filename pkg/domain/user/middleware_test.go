package user

import (
	"context"
	"testing"
	"time"
)

func TestMiddlewareForwardsAuthRequest(t *testing.T) {
	svc := &fakeService{}
	m := NewServiceMiddleware(svc)
	r, err := m.Authenticate(context.Background(), "foo", "FooToken")
	if err != nil {
		t.Errorf("Unexpected error on auth, err %v", err)
	}

	if !r {
		t.Error("Unexpected result")
	}
	if !svc.authCalled {
		t.Error("Unexpected result")
	}
}

func TestMiddlewareForwardsRegisterRequest(t *testing.T) {
	svc := &fakeService{}
	m := NewServiceMiddleware(svc)
	u := &User{
		ID:      "b304623a-6476-4991-8476-3b62a29484c4 ",
		Token:   "Hashed Token",
		Name:    "fooName",
		State:   ENABLED,
		Role:    ADMIN,
		Created: time.Now(),
	}
	err := m.Register(context.Background(), u)
	if err != nil {
		t.Errorf("Unexpected error on auth, err %v", err)
	}

	if !svc.registerCalled {
		t.Error("Unexpected result")
	}
}

type fakeService struct {
	authCalled, registerCalled bool
}

func (f *fakeService) Authenticate(ctx context.Context, ID string, token string) (bool, error){
	f.authCalled = true
	return true, nil
}

func (f *fakeService) Register(ctx context.Context, user *User) error{
	f.registerCalled = true
	return nil
}

func (f *fakeService) GetUser(ctx context.Context, ID string) (*User, error){
	return nil, nil
}
func (f *fakeService) GetAll(ctx context.Context) ([]*User, error){
	return nil, nil
}
func (f *fakeService) Update(ctx context.Context, user *User) error{
	return nil
}
func (f *fakeService) Delete(ctx context.Context, ID string) error{
	return nil
}