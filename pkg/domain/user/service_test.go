package user

import (
	"context"
	"testing"
)

func TestAuthenticateSuccessOnValidCredentials(t *testing.T) {
	uid := "FooBarUID"
	token := "fakeToken"

	repo := &FakeRepository{token: token}
	svc := NewService(repo)

	res, err := svc.Authenticate(context.Background(), uid, token)
	if err != nil {
		t.Fatal("Unexpected result")
	}

	if !res {
		t.Fatal("Unexpected result")
	}
}

func TestAuthenticateFailsOnInValidCredentials(t *testing.T) {
	uid := "FooBarUID"
	token := "fakeToken"

	repo := &FakeRepository{token: token}
	svc := NewService(repo)

	res, err := svc.Authenticate(context.Background(), uid, token)
	if err != nil {
		t.Fatal("Unexpected result")
	}

	if !res {
		t.Fatal("Unexpected result")
	}
}

type FakeRepository struct {
	token string
}

func (f *FakeRepository) GetByUid(ctx context.Context, uid string) (*User, error) {
	return &User{ID: uid, Token: f.token}, nil
}
func (f *FakeRepository) GetAll(ctx context.Context) ([]*User, error) {
	return nil, nil
}
func (f *FakeRepository) Add(ctx context.Context, u *User) error {
	return nil
}
func (f *FakeRepository) Update(ctx context.Context, u *User) error {
	return nil
}
func (f *FakeRepository) Delete(ctx context.Context, uid string) error {
	return nil
}
