package user

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
)

var ErrNoUserFound = errors.New("no user found")

type SQLRepository struct {
	db *sqlx.DB
}

func NewSQLRepository(db *sqlx.DB) *SQLRepository {
	return &SQLRepository{db: db}
}

func (r *SQLRepository) GetByUid(ctx context.Context, ID string) (*User, error) {
	rows, err := r.db.QueryContext(ctx, "SELECT id, token, username, state, role, created FROM users WHERE id = $1", ID)
	if err != nil {
		return nil, err
	}

	users, err := r.populate(rows)
	if err != nil {
		return nil, err
	}
	if len(users) == 0 {
		return nil, ErrNoUserFound
	}

	return users[0], nil
}

// getAll returns a list of all the items in the catalog
func (r *SQLRepository) GetAll(ctx context.Context) ([]*User, error) {
	rows, err := r.db.QueryContext(ctx, "SELECT id, token, username, state, role, created FROM users ORDER BY id ASC")
	if err != nil {
		return nil, err
	}

	return r.populate(rows)
}

// Add a new item to the catalog
func (r *SQLRepository) Add(ctx context.Context, i *User) error {
	const sql = `INSERT INTO users (id, token, username, state, role, created) VALUES ($1, $2, $3, $4, $5, $6)`
	_, err := r.db.ExecContext(ctx, sql, i.ID, i.Token, i.Name, i.State, i.Role, i.Created)
	return err
}

// Update the information of an item  //@TODO: Add test
func (r *SQLRepository) Update(ctx context.Context, i *User) error {
	const sqlstr = `UPDATE users SET token=$1, username=$2, state=$3,  role=$4, created=$5 WHERE id = $6`
	_, err := r.db.ExecContext(ctx, sqlstr, i.Token, i.Name, i.State, i.Role, i.Created, i.ID)

	return err
}

// Delete the item with the given SKU from the catalog
func (r *SQLRepository) Delete(ctx context.Context, ID string) error {
	const sqlstr = `DELETE FROM users WHERE id = $1`
	_, err := r.db.ExecContext(ctx, sqlstr, ID)
	return err
}

func (r *SQLRepository) populate(rows *sql.Rows) ([]*User, error) {
	defer rows.Close()

	users := []*User{}
	for rows.Next() {
		var u = &User{}
		if err := rows.Scan(&u.ID, &u.Token, &u.Name, &u.State, &u.Role,&u.Created); err != nil {
			return nil, fmt.Errorf("unexpected error populating users, err %s", err)
		}

		users = append(users, u)
	}

	if len(users) == 0 {
		return nil, ErrNoUserFound
	}

	return users, nil
}
