package user

import (
	"context"
	log "github.com/sirupsen/logrus"
)

type ServiceMiddleware struct {
	service Service
}

func NewServiceMiddleware(srv Service) *ServiceMiddleware {
	return &ServiceMiddleware{
		service: srv,
	}
}

func (s *ServiceMiddleware)GetUser(ctx context.Context, ID string) (*User, error){
	log.Infof("GetUser uid %s", ID)

	return s.service.GetUser(ctx, ID)
}

func (s *ServiceMiddleware)GetAll(ctx context.Context) ([]*User, error){
	log.Info("GetAll Users ")

	return s.service.GetAll(ctx)
}

func (s *ServiceMiddleware) Authenticate(ctx context.Context, uid string, token string) (bool, error) {
	log.Infof("Authenticate uid %s", uid)
	r, err := s.service.Authenticate(ctx, uid, token)
	if err != nil {
		log.Errorf("Track Authenticate error on user %s token %s err %v", uid, token, err)
	}

	if r {
		log.Debugf("Track Successful Authenticate request on user %s token %s", uid, token)
	} else {
		log.Debugf("Track UnSuccessful Authenticate request  on user %s token %s", uid, token)
	}

	return r, err
}

func (s *ServiceMiddleware) Register(ctx context.Context, user *User) error {
	log.Infof("Register uid %s token %s",user.ID, user.Token)
	err := s.service.Register(ctx, user)
	if err != nil {
		log.Errorf("Track Register error on user %s token %s err %v", user.ID, user.Token, err)
		return err
	}

	log.Debugf("Track Register success on user %s token %s", user.ID, user.Token)

	return nil
}

func (s *ServiceMiddleware) Update(ctx context.Context, user *User) error {
	log.Infof("Update uid %s token %s",user.ID, user.Token)
	return s.service.Update(ctx, user)
}

func (s *ServiceMiddleware) Delete(ctx context.Context, ID string) error {
	log.Infof("Delete uid %s",ID)
	return s.service.Delete(ctx, ID)
}
