package handler

import (
	gprc "bitbucket.org/marcosQuesada/common/transport/grpc/user"
	domain "bitbucket.org/marcosQuesada/user-service/pkg/domain/user"
	"context"
	log "github.com/sirupsen/logrus"
	"time"
)

type User struct {
	service domain.Service
}

func NewUser(service domain.Service) *User {
	return &User{service: service}
}

func (u *User) GetUser(ctx context.Context, request *gprc.UserRequest) (*gprc.UserResponse, error) {
	r, err := u.service.GetUser(ctx, request.ID)
	if err != nil {
		log.Errorf("GRPC GetUser Error, err %v", err)
		return &gprc.UserResponse{ID: request.ID, Success: false}, nil
	}

	return &gprc.UserResponse{ID: request.ID, Success: true, User: ConvertFromDomain(r)}, nil
}

func (u *User) GetAll(ctx context.Context, request *gprc.AllUserRequest) (*gprc.AllUserResponse, error) {
	r, err := u.service.GetAll(ctx)
	if err != nil {
		log.Errorf("GRPC GetAll Error, err %v", err)
		return &gprc.AllUserResponse{Success: false}, nil
	}
	users := make([]*gprc.User, 0)
	for _, user := range r {
		users = append(users, ConvertFromDomain(user))
	}

	return &gprc.AllUserResponse{Success: true, Users: users}, nil
}

func (u *User) Authenticate(ctx context.Context, request *gprc.AuthRequest) (*gprc.UserResponse, error) {
	r, err := u.service.Authenticate(ctx, request.ID, request.Token)
	if err != nil {
		log.Errorf("GRPC Authenticating Error, err %v", err)
		return &gprc.UserResponse{ID: request.ID, Success: false}, nil
	}

	return &gprc.UserResponse{ID: request.ID, Success: r}, nil
}

func (u *User) Register(ctx context.Context, request *gprc.RegisterRequest) (*gprc.RegisterResponse, error) {
	err := u.service.Register(ctx, ConvertFromProtocol(request.User))
	if err != nil {
		log.Errorf("GRPC Register Error, err %v", err)
		return &gprc.RegisterResponse{ID: request.User.ID, Success: false}, nil
	}

	return &gprc.RegisterResponse{ID: request.User.ID, Success: true}, nil
}

func (u *User) Update(ctx context.Context, request *gprc.UpdateRequest) (*gprc.UpdateResponse, error) {
	err := u.service.Update(ctx, ConvertFromProtocol(request.User))
	if err != nil {
		log.Errorf("GRPC Update Error, err %v", err)
		return &gprc.UpdateResponse{ID: request.User.ID, Success: false}, nil
	}

	return &gprc.UpdateResponse{ID: request.User.ID, Success: true}, nil
}

func (u *User) Delete(ctx context.Context, request *gprc.UserRequest) (*gprc.DeleteResponse, error) {
	err := u.service.Delete(ctx, request.ID)
	if err != nil {
		log.Errorf("GRPC Delete Error, err %v", err)
		return &gprc.DeleteResponse{Success: false}, nil
	}

	return &gprc.DeleteResponse{ID: request.ID, Success: true}, nil
}

func ConvertFromDomain(e *domain.User) *gprc.User {
	return &gprc.User{
		ID:      e.ID,
		Token:   e.Token,
		Name:    e.Name,
		State:   int64(e.State),
		Role:    int64(e.Role),
		Created: e.Created.UnixNano(),
	}
}

func ConvertFromProtocol(e *gprc.User) *domain.User {
	return &domain.User{
		ID:      e.ID,
		Token:   e.Token,
		Name:    e.Name,
		State:   domain.StateType(int(e.State)),
		Role:    domain.RoleType(int(e.Role)),
		Created: time.Unix(0, e.Created),
	}
}
