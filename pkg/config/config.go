package config

import (
	cfg "bitbucket.org/marcosQuesada/common/config"
)

type Config struct {
	Postgres cfg.Postgres `config:"postgress"`
	Grpc     cfg.Grpc     `config:"grpc"`
}
