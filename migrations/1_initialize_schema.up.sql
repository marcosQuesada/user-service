
CREATE TABLE users (
  id VARCHAR(128) PRIMARY KEY,
  token VARCHAR(255) NOT NULL,
  username VARCHAR(255) NOT NULL,
  state INTEGER NOT NULL,
  role INTEGER NOT NULL,
  created TIMESTAMP DEFAULT current_timestamp NOT NULL
);
CREATE UNIQUE INDEX user_id_idx ON users (id);

INSERT INTO users (id, token, username, state, role, created)
    VALUES ('af2acb39-5f44-4e1b-a045-41988f9b94df', 'HashedPass', 'fooName', 0, 5, current_timestamp);

INSERT INTO users (id, token, username, state, role, created)
    VALUES ('ff2acb39-5f44-4e1b-a045-41988f9b94df', 'HashedPass', 'fooName', 0, 1, current_timestamp);
